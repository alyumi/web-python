access_mode_template = [
    "switchport mode access", "switchport access vlan",
    "switchport nonegotiate", "spanning-tree portfast",
    "spanning-tree bpduguard enable"
]

access_config = {
    "FastEthernet0/12": 10,
    "FastEthernet0/14": 11,
    "FastEthernet0/16": 17
}

port_security_template = [
    "switchport port-security maximum 2",
    "switchport port-security violation restrict",
    "switchport port-security"
]


def generate_access_config(intf_vlan_mapping, access_template, port_security_template=None):
    """
    intf_vlan_mapping - словарь с соответствием интерфейс-VLAN такого вида:
        {"FastEthernet0/12": 10,
         "FastEthernet0/14": 11,
         "FastEthernet0/16": 17}
    access_template - список команд для порта в режиме access

    Возвращает список всех портов в режиме access с конфигурацией на основе шаблона
    """

    for elem in intf_vlan_mapping:
        print('interface {}'.format(elem))
        for template in access_template:
            if template.split()[-1] == 'vlan':
                print(template, intf_vlan_mapping.get(elem))
            else:
                print(template)
        if port_security_template != None:
            for elem in port_security_template:
                print(elem)



def main():
    generate_access_config(access_config, access_mode_template, port_security_template=port_security_template)
    generate_access_config(access_config, access_mode_template)

if __name__ == "__main__":
    main()

