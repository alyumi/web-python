"""
Задание 11.2
Создать функцию create_network_map, которая обрабатывает
вывод команды show cdp neighbors из нескольких файлов и объединяет его в одну общую топологию.
У функции должен быть один параметр filenames, который ожидает как аргумент список с именами файлов, в которых находится вывод команды show cdp neighbors.
Функция должна возвращать словарь, который описывает соединения между устройствами.
Структура словаря такая же, как в задании 11.1:
    {("R4", "Fa0/1"): ("R5", "Fa0/1"),
     ("R4", "Fa0/2"): ("R6", "Fa0/0")}
Cгенерировать топологию, которая соответствует выводу из файлов:
* sh_cdp_n_sw1.txt
* sh_cdp_n_r1.txt
* sh_cdp_n_r2.txt
* sh_cdp_n_r3.txt
В словаре, который возвращает функция create_network_map, не должно быть дублей.
С помощью функции draw_topology из файла draw_network_graph.py нарисовать схему на основании топологии, полученной с помощью функции create_network_map.
Результат должен выглядеть так же, как схема в файле task_11_2a_topology.svg
При этом:
* Расположение устройств на схеме может быть другим
* Соединения должны соответствовать схеме
Не копировать код функций parse_cdp_neighbors и draw_topology. Если функция parse_cdp_neighbors не может обработать вывод
одного из файлов с выводом команды, надо исправить код функции в задании 11.1.
Ограничение: Все задания надо выполнять используя только пройденные темы.
> Для выполнения этого задания, должен быть установлен graphviz:
> apt-get install graphviz
> И модуль python для работы с graphviz:
> pip install graphviz
"""

from web_python_2020 import draw_network_graph

def create_network_map(filenames):
    pass

def parse_cdp_neighbors(command_output):
    """
    Тут мы передаем вывод команды одной строкой потому что именно в таком виде
    будет получен вывод команды с оборудования. Принимая как аргумент вывод
    команды, вместо имени файла, мы делаем функцию более универсальной: она может
    работать и с файлами и с выводом с оборудования.
    Плюс учимся работать с таким выводом.
    """
    flag = 0
    command_dict = dict()

    for elem in command_output.split('\n'):
        if len(elem) > 0:
            for ele in elem.split():
                if ele == 'ID':
                    flag = flag + 1

            if flag == 2:
                flag = flag + 1
                continue

            if flag == 3:
                command_dict.update({(elem.split()[0], str(elem.split()[1] + elem.split()[2]))
                                    : (elem.split()[0], str(elem.split()[-2] + elem.split()[-1]))})

    return command_dict

if __name__ == "__main__":
    infiles = [
        "sh_cdp_n_sw1.txt",
        "sh_cdp_n_r1.txt",
        "sh_cdp_n_r2.txt",
        "sh_cdp_n_r3.txt",
    ]

    topology = create_network_map(infiles)
    # рисуем топологию:
    # draw_topology(topology)
