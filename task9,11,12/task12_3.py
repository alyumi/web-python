import subprocess
from tabulate import tabulate


ips = ['127.0.0.1-10', '0.0.0.1', '198.168.1.1-198.168.1.10', '72.11.5.19']
alive = []
unreachable = []

def print_ip_table(alive, unreachable):
    colums = ['Alive', 'Unreachable']
    list_of_ips = []
    for i in range(min(len(alive), len(unreachable))):
        list_of_ips.append((alive[i],  unreachable[i]))

    if len(alive) >= len(unreachable):
        for i in range(len(unreachable), len(alive)):
            list_of_ips.append((alive[i], ''))
    elif len(alive) < len(unreachable):
        for i in range(len(alive), len(unreachable)):
            list_of_ips.append(('', unreachable[i]))

    print(tabulate(list_of_ips, headers=colums, tablefmt="grid"))

def convert_ranges_to_ip_list(rann):
    newrange = []
    for ran in rann:
        index = ran.find('-')
        if index != -1:
            first = ran.rfind('.', 0, index)
            last = ran.rfind('.', 0, -1)

            base = ran[0:first + 1]
            start = (ran[first + 1:index])
            if first == last:
                end = ran[index + 1::]
            elif first != last:
                end = ran[ran.rfind('.', index, -1) + 1::]

            for i in range(int(start), int(end) + 1):
                newrange.append(base + str(i))
        else:
            newrange.append(ran)
    return newrange

def ping_ip_addresses(ips):
	for ip in ips:
		reply = subprocess.run(['ping', '-n', '3',  ip])
		if reply.returncode == 0:
			alive.append(ip)
		else:
			unreachable.append(ip)

	tuple(alive)
	tuple(unreachable)

	return alive, unreachable

ping_ip_addresses(convert_ranges_to_ip_list(ips))
print_ip_table(alive, unreachable)