"""
Задание 9.3
Создать функцию get_int_vlan_map, которая обрабатывает конфигурационный файл коммутатора
и возвращает кортеж из двух словарей:
* словарь портов в режиме access, где ключи номера портов, а значения access VLAN (числа):
{'FastEthernet0/12': 10,
 'FastEthernet0/14': 11,
 'FastEthernet0/16': 17}
* словарь портов в режиме trunk, где ключи номера портов, а значения список разрешенных VLAN (список чисел):
{'FastEthernet0/1': [10, 20],
 'FastEthernet0/2': [11, 30],
 'FastEthernet0/4': [17]}
У функции должен быть один параметр config_filename, который ожидает как аргумент имя конфигурационного файла.
Проверить работу функции на примере файла config_sw1.txt
Ограничение: Все задания надо выполнять используя только пройденные темы.
"""

from sys import argv

filename = argv[1]

def get_int_vlan_map(config_filename):
    file = open(config_filename, 'r')
    access = dict()
    trunk = dict()
    inte = str()
    for string in file:
        if string.split()[0] == 'interface':
            inte = string.split()[1]
        if string.split()[0] == 'switchport':
            if string.split()[-2] == 'vlan':
                if string.split()[1] == 'access':
                    access.update({inte: string.split()[-1].split(',')})
                elif string.split()[1] == 'trunk':
                    trunk.update({inte: string.split()[-1].split(',')})

    print(access)
    print(trunk)


def main():
    get_int_vlan_map(filename)

if __name__ == '__main__':
    main()