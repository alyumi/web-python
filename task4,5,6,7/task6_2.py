print('enter ip: ')
ip = str(input())

okt = ip.split('.')
num_okt = []

for elem in okt:
    num_okt.append(int(elem))

if (num_okt[0] == 0) and (num_okt[1] == 0) and (num_okt[2] == 0) and (num_okt[3] == 0):
    print('unassigned')
elif (num_okt[0] == 255) and (num_okt[1] == 255) and (num_okt[2] == 255) and (num_okt[3] == 255):
    print('local broadcast')
elif (num_okt[0] >= 1) and (num_okt[0] <= 223):
    print('unicast')
elif (num_okt[0] >= 224) and (num_okt[0] <= 239):
    print('multicast')
else:
    print('unused')