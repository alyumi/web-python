bigf = 0
while bigf == 0:
    print('enter ip: ')
    ip = str(input())

    try:
        okt = ip.split('.')
    except Exception as e:
        print('Wrong!')
    else:
        flag = 0
        for elem in okt:
            if not elem.isdigit():
                flag = 1
                break
            if int(elem) < 0 or int(elem) > 255:
                flag = 1
                break

        if flag == 0:
            num_okt = []

            for elem in okt:
                num_okt.append(int(elem))

            if (num_okt[0] == 0) and (num_okt[1] == 0) and (num_okt[2] == 0) and (num_okt[3] == 0):
                print('unassigned')
            elif (num_okt[0] == 255) and (num_okt[1] == 255) and (num_okt[2] == 255) and (num_okt[3] == 255):
                print('local broadcast')
            elif (num_okt[0] >= 1) and (num_okt[0] <= 223):
                print('unicast')
            elif (num_okt[0] >= 224) and (num_okt[0] <= 239):
                print('multicast')
            else:
                print('unused')
            bigf = 1
        else:
            print('Wrong')